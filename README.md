# Projet Kinect

## Introduction
Ce projet ambitieux est entrepris dans le cadre de notre 3e année de BUT, avec pour objectif de développer une application de bureau innovante qui exploite les capacités avancées des capteurs de la Kinect.

Nous avons donc pour **but** de réaliser un jeu interactif développé en C# et WPF, spécifiquement conçu pour l'utilisation avec la Kinect. Le démarrage du jeu est initié par un clic sur un bouton, ce qui lance une musique d'ambiance et déclenche un compte à rebours de trois secondes précédant le lancement effectif de la partie. Une fois cette phase introductive passée, le jeu fait apparaître des cercles à des positions aléatoires sur l'écran, avec pour consigne de ne pas dépasser un maximum de cinq cercles simultanément. Ces cercles commencent à s'estomper lentement, laissant ainsi le temps au joueur, détecté préalablement par la Kinect, de réaliser un geste spécifique : frapper des mains (ClapHands) à l'emplacement des cercles. L'exécution réussie de ce geste entraîne l'élimination des cercles et l'attribution de points au joueur, le score final dépendant de la rapidité avec laquelle les cercles sont supprimés.

Voilà voilà pour ce qui est du jeu qu'on souhaite mettre en place.

## Technologies Utilisées
- **Langage de Programmation**: C#
- **Framework**:
    - Windows Presentation Foundation (WPF)
    - .NET Framework

## Prérequis
- **Matériel**: Une Kinect est nécessaire pour tester le projet.
- **Logiciel**: Visual Studio pour exécuter et développer le projet WPF.

## Utilisation
1. **Cloner le Répertoire**: Clonez le dépôt GitHub à l'aide de `git clone [url-du-dépôt]`.
2. **Ouvrir avec Visual Studio**: Lancez Visual Studio et ouvrez le projet cloné.
3. **Installer les Dépendances**: Assurez-vous que toutes les bibliothèques nécessaires sont installées. *(n'oubliez pas le SDK de Kinect v2.0)*
4. **Connecter la Kinect**: Branchez votre Kinect à votre ordinateur.
5. C'est parti, vous pouvez **lancer l'application WPF**

## Documentation
### Ce qui marche actuellement
- TP1: Application bureau WPF qui permet d'utiliser les différents flux de la Kinect + une ellipse avec un texte qui permet de référer l'état de la Kinect.
- TP2: Application console qui permet de reconnaître les postures et gestes d'une personne.

    Les postures:
    - `PostureHandsOnHead` -> les deux **mains** sur la tête
    - `PostureHandUp` -> **Main levée** au-dessus de la tête
    - `RightHandUp` -> **Main droite levée** au-dessus de la tête

    Les **gestes**:
    - `SwipeRightHand` -> l'avant-bras au niveau du torse qui balaye/Swipe sur la droite
    - `ClapHands` -> Lorsque les mains se rejoignent pour claper

### Ce qui ne marche pas actuellement
- TP3: Réaliser le jeu décrit précédemment dans l'introduction. Nous sommes parvenus à récupérer le nom des gestes détectés lors de la capture d'images sur l'application WPF. Cependant, le problème réside dans le fait que nous n'avons pas réussi à implémenter le jeu proprement dit, c'est-à-dire que lorsqu'un clap est détecté, cela ne supprime pas les bulles qui apparaissent à l'écran. De plus, une grande partie de l'interface graphique n'est pas encore terminée, notamment les bulles qui viennent parasiter le milieu de l'écran.
Toutefois, si vous souhaitez voir jusqu'où nous sommes allés, la branche [Début-Tp3](https://codefirst.iut.uca.fr/git/louis.dufour/Kinect-Project/src/branch/D%C3%A9but-Tp3) est disponible