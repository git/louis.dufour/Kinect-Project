﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WpfApp
{
    public class Bone : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        private double _top;
        public double Top
        {
            get { return _top; }
            set
            {
                _top = value;
                OnPropertyChanged(nameof(Top));
            }
        }
        public double Left { get; set; }

        public double Diameter
        {
            get { return _diameter; }
            set
            {
                _diameter = value;
                OnPropertyChanged(nameof(Diameter));
            }
        }
        private double _diameter;
        public SolidColorBrush ColorBrush { get; set; }
    }
}
