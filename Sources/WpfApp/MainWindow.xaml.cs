﻿using Lib;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private KinectManager kinectManager;
        public KinectManager KinectManager
        {
            get { return kinectManager; }
            set { kinectManager = value; }
        }
        private KinectStream _currentKinectStream;
        public KinectStream CurrentKinectStream
        {
            get { return _currentKinectStream; }
            set
            {
                _currentKinectStream = value;
                OnPropertyChanged(nameof(CurrentKinectStream));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private KinectStreamsFactory _factory;
        public KinectStreamsFactory Factory
        {
            get { return _factory; }
            set { _factory = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            KinectManager = new KinectManager();
            Factory = new KinectStreamsFactory(KinectManager, skeletonCanvas);
            CurrentKinectStream = Factory[KinectStreams.Color];
            Debug.WriteLine(CurrentKinectStream.KinectManager.StatusText);
            CurrentKinectStream.Start();
            Debug.WriteLine(CurrentKinectStream.KinectManager.StatusText);
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Debug.WriteLine(CurrentKinectStream.KinectManager.StatusText);
            CurrentKinectStream.Stop();
        }

        private void ToColorImageStream(object sender, RoutedEventArgs e)
        {
            CurrentKinectStream.Stop();
            CurrentKinectStream = Factory[KinectStreams.Color];
            Debug.WriteLine(CurrentKinectStream.GetType().Name);
            CurrentKinectStream.Start();
        }

        private void ToDepthImageStream(object sender, RoutedEventArgs e)
        {
            CurrentKinectStream.Stop();
            CurrentKinectStream = Factory[KinectStreams.Depth];
            Debug.WriteLine(CurrentKinectStream.GetType().Name);
            CurrentKinectStream.Start();
        }

        private void ToInfraredImageStream(object sender, RoutedEventArgs e)
        {
            CurrentKinectStream.Stop();
            CurrentKinectStream = Factory[KinectStreams.IR];
            Debug.WriteLine(CurrentKinectStream.GetType().Name);
            CurrentKinectStream.Start();
        }

        private void ToBodyImageStream(object sender, RoutedEventArgs e)
        {
            CurrentKinectStream.Stop();
            CurrentKinectStream = Factory[KinectStreams.Body];
            Debug.WriteLine(CurrentKinectStream.GetType().Name);
            CurrentKinectStream.Start();
        }

        private void ToColorAndBodyImageStream(object sender, RoutedEventArgs e)
        {
            CurrentKinectStream.Stop();
            CurrentKinectStream = Factory[KinectStreams.ColorAndBody];
            Debug.WriteLine(CurrentKinectStream.GetType().Name);
            CurrentKinectStream.Start();
        }
    }
}
