﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Lib;

namespace WpfApp
{
    class StreamTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ColorAndBodyImageStreamTemplate { get; set; }
        public DataTemplate OtherStreamTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is BodyImageStream)
                return ColorAndBodyImageStreamTemplate;
            else
                return OtherStreamTemplate;
        }
    }
}
