﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectUtils
{
    public abstract class Gesture : BaseGesture
    {
        public bool IsRecognitionRunning { get; set; } = false;

        protected int MinNbOfFrames = 10; // Exemple de valeur, ajustez selon le geste
        protected int MaxNbOfFrames = 50; // Exemple de valeur, ajustez selon le geste
        private int currentFrameCount = 0;

        public override void TestGesture(Body body)
        {
            if (!IsRecognitionRunning)
            {
                if (TestInitialConditions(body))
                {
                    IsRecognitionRunning = true;
                    currentFrameCount = 0;
                }
            }
            else
            {
                currentFrameCount++;

                if (
                    !TestPosture(body)
                    || !TestRunningGesture(body)
                    || currentFrameCount > MaxNbOfFrames
                )
                {
                    IsRecognitionRunning = false;
                }
                else if (TestEndConditions(body) && currentFrameCount >= MinNbOfFrames)
                {
                    OnGestureRecognized(body);
                    Console.WriteLine(GestureName);
                    IsRecognitionRunning = false;
                }
            }
        }

        protected abstract bool TestInitialConditions(Body body);
        protected abstract bool TestPosture(Body body);
        protected abstract bool TestRunningGesture(Body body);
        protected abstract bool TestEndConditions(Body body);
    }
}
