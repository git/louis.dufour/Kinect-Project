﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectUtils
{
    public abstract class Posture : BaseGesture
    {
        // Déclaration des événements
        public event Action<Body> PostureRecognized;
        public event Action<Body> PostureUnrecognized;

        private bool wasRecognized = false; // Pour garder l'état de reconnaissance précédent

        protected abstract bool TestPosture(Body body);

        public override void TestGesture(Body body)
        {
            bool isRecognized = TestPosture(body); // Teste la posture actuelle

            if (isRecognized && !wasRecognized)
            {
                // La posture est reconnue et ne l'était pas au frame précédent
                PostureRecognized?.Invoke(body);
                Console.WriteLine(GestureName);
            }
            else if (!isRecognized && wasRecognized)
            {
                // La posture n'est plus reconnue alors qu'elle l'était au frame précédent
                PostureUnrecognized?.Invoke(body);
            }

            wasRecognized = isRecognized; // Mise à jour de l'état de reconnaissance pour le prochain frame
        }
    }
}
