﻿using System;
using Microsoft.Kinect;

namespace KinectUtils
{
    public abstract class BaseGesture
    {
        // Événement déclenché lorsque le geste est reconnu
        public event EventHandler GestureRecognized;

        // Nom du geste - marqué comme virtual pour permettre la substitution
        public virtual string GestureName { get; protected set; }

        // Méthode abstraite pour tester le geste
        public abstract void TestGesture(Body body);

        // Méthode protégée pour déclencher l'événement GestureRecognized
        protected virtual void OnGestureRecognized(Body body)
        {
            GestureRecognized?.Invoke(this, new GestureRecognizedEventArgs(body, GestureName));
        }
    }

    public class GestureRecognizedEventArgs : EventArgs
    {
        public Body Body { get; private set; }
        public string GestureName { get; private set; }

        public GestureRecognizedEventArgs(Body body, string gestureName)
        {
            Body = body;
            GestureName = gestureName;
        }
    }
}
