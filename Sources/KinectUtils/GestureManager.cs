﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Lib;
using Microsoft.Kinect;

namespace KinectUtils
{
    public static class GestureManager
    {
        private static BodyFrameReader bodyFrameReader;
        private static Body[] bodies;

        static event EventHandler GestureRecognized;
        public static event EventHandler BodyFrameArrived;
        public static event EventHandler<ProcessedBodyFrameEventArgs> ProcessedBodyFrameArrived;


        static KinectManager KinectManager { get; set; }
        static List<BaseGesture> KnownGestures { get; set; } = new List<BaseGesture>();

        public static void AddGestures(IGestureFactory factory)
        {
            var gestures = (List<BaseGesture>)factory.CreateGestures();
            foreach (var gesture in gestures)
            {
                AddGesture(gesture);
            }
        }

        public static void AddGesture(BaseGesture gesture)
        {
            if (!KnownGestures.Contains(gesture))
            {
                KnownGestures.Add(gesture);
                gesture.GestureRecognized += (sender, e) => GestureRecognized?.Invoke(sender, e);
            }
        }

        public static void RemoveGesture(BaseGesture gesture)
        {
            if (KnownGestures.Contains(gesture))
            {
                KnownGestures.Remove(gesture);
                gesture.GestureRecognized -= (sender, e) => GestureRecognized?.Invoke(sender, e);
            }
        }

        public static void StartAcquiringFrames(KinectManager kinectManager)
        {
            bodyFrameReader = kinectManager.Sensor.BodyFrameSource.OpenReader();
            bodyFrameReader.FrameArrived += Reader_BodyFrameArrived;
        }

        private static void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            using (var bodyframe = e.FrameReference.AcquireFrame())
            {
                if (bodyframe != null)
                {
                    if (bodies == null)
                    {
                        bodies = new Body[bodyframe.BodyCount];
                    }
                    bodyframe.GetAndRefreshBodyData(bodies);

                    Body closestBody = bodies.Where(b => b.IsTracked)
                                             .OrderBy(b => b.Joints[JointType.SpineBase].Position.Z)
                                             .FirstOrDefault();

                    ProcessedBodyFrameArrived?.Invoke(sender, new ProcessedBodyFrameEventArgs(bodies, closestBody));

                    foreach (var body in bodies)
                    {
                        if (body.IsTracked)
                        {
                            foreach (var gesture in KnownGestures)
                            {
                                gesture.TestGesture(body);
                            }
                        }
                    }
                    
                }
            }
        }
    }
}
