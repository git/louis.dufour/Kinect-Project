﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectUtils
{
    public abstract class BaseMapping<T>
    {
        protected bool running = false;
        public event EventHandler<OnMappingEventArgs<T>> OnMapping;

        protected BaseMapping(BaseGesture startGesture, BaseGesture endGesture)
        {
            SubscribeToStartGesture(startGesture);
            SubscribeToEndGesture(endGesture);
            // Assuming GestureManager has an event that provides processed body data
            GestureManager.ProcessedBodyFrameArrived += OnProcessedBodyFrameArrived;
        }

        protected abstract T Mapping(Body body);

        protected bool TestMapping(Body body, out T output)
        {
            output = default(T);
            if (running)
            {
                output = Mapping(body);
                return true;
            }
            return false;
        }

        protected void SubscribeToStartGesture(BaseGesture gesture)
        {
            gesture.GestureRecognized += (sender, e) => { if (!running) running = true; };
        }

        protected void SubscribeToEndGesture(BaseGesture gesture)
        {
            gesture.GestureRecognized += (sender, e) => { if (running) running = false; };
        }

        // Assuming your GestureManager provides a similar event
        protected void OnProcessedBodyFrameArrived(object sender, ProcessedBodyFrameEventArgs e)
        {
            foreach (var body in e.Bodies)
            {
                if (body.IsTracked)
                {
                    T output;
                    if (TestMapping(body, out output))
                    {
                        OnMapping?.Invoke(this, new OnMappingEventArgs<T>(output));
                    }
                }
            }
        }
    }

    public class OnMappingEventArgs<T> : EventArgs
    {
        public T MappedValue { get; private set; }

        public OnMappingEventArgs(T mappedValue)
        {
            MappedValue = mappedValue;
        }
    }

    // Custom EventArgs to be used within your application logic
    // This is not from the Kinect SDK, but rather a custom class to pass along processed body data
    public class ProcessedBodyFrameEventArgs : EventArgs
    {
        public Body[] Bodies { get; private set; }
        public Body ClosestBody { get; private set; }

        public ProcessedBodyFrameEventArgs(Body[] bodies, Body closestBody)
        {
            Bodies = bodies;
            ClosestBody = closestBody;
        }
    }
}
