﻿using KinectUtils;
using Lib;
using Microsoft.Kinect;
using MyGesturesBank;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // allumer la kinect
            KinectManager kinectManager = new KinectManager();
            kinectManager.StartSensor();
            Console.WriteLine("Kinect démarre");

            AllGesturesFactory allGesturesFactory = new AllGesturesFactory();
            GestureManager.AddGestures(allGesturesFactory);
            GestureManager.StartAcquiringFrames(kinectManager);
            while (true)
            {
                Thread.Sleep(50);
            }
        }
    }
}
