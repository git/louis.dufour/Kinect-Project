﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinectUtils;

namespace MyGesturesBank
{
    public class AllGesturesFactory : IGestureFactory
    {
        public IEnumerable<BaseGesture> CreateGestures()
        {
            return new List<BaseGesture>
            {
                new SwipeRightHand(),
                new PostureHandsOnHead(),
                new PostureHandUp(),
                new RightHandUp(),
                new ClapHands(),
                // Ajoutez d'autres gestes ici
            };
        }
    }
}
