﻿using KinectUtils;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGesturesBank
{
    public class SwipeRightHand : Gesture
    {
        private float previousX = float.NaN;
        private bool gestureStarted = false;

        protected override bool TestInitialConditions(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var shoulderRight = body.Joints[JointType.ShoulderRight].Position;

            // Conditions initiales : main droite au niveau ou à droite de l'épaule droite, mais pas trop éloignée
            bool initialCondition = handRight.X >= shoulderRight.X && handRight.X - shoulderRight.X < 0.4f;

            if (initialCondition && !gestureStarted)
            {
                // Si les conditions initiales sont remplies et que le geste n'a pas encore commencé,
                // initialiser previousX et marquer le geste comme commencé
                previousX = handRight.X;
                gestureStarted = true;
                return false; // Attendre le prochain frame pour commencer l'évaluation
            }
            return initialCondition;
        }

        protected override bool TestPosture(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var head = body.Joints[JointType.Head].Position;

            // La main droite ne doit pas être plus haute que la tête
            return handRight.Y <= head.Y;
        }

        protected override bool TestRunningGesture(Body body)
        {
            if (!gestureStarted) return false; // Assurer que le geste a commencé correctement

            var handRight = body.Joints[JointType.HandRight].Position.X;

            if (!float.IsNaN(previousX))
            {
                // Vérifie si la main droite se déplace vers la droite
                bool isMovingRight = handRight > previousX;
                previousX = handRight;
                return isMovingRight;
            }

            previousX = handRight;
            return false;
        }

        protected override bool TestEndConditions(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var spineBase = body.Joints[JointType.SpineBase].Position;

            // Condition de fin : la main droite est bien à droite de la base de la colonne vertébrale
            if (handRight.X > spineBase.X + 0.8f) // Ajustez cette valeur selon le besoin
            {
                gestureStarted = false; // Réinitialiser l'état du geste
                previousX = float.NaN; // Préparer pour la prochaine détection
                return true;
            }
            return false;
        }

        public override string GestureName => "Swipe Right Hand";
    }
}
