﻿using Microsoft.Kinect;
using KinectUtils;

namespace MyGesturesBank
{
    public class PostureHandUp : Posture
    {
        protected override bool TestPosture(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var handLeft = body.Joints[JointType.HandLeft].Position;
            var head = body.Joints[JointType.Head].Position;

            return handRight.Y > head.Y || handLeft.Y > head.Y;
        }

        public override string GestureName => "Posture Hand Up";
    }
}
