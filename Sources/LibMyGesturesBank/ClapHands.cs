﻿using KinectUtils;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MyGesturesBank
{
    public class ClapHands : Gesture
    {
        private CameraSpacePoint previousRightHandPosition;
        private CameraSpacePoint previousLeftHandPosition;
        private bool isClapInitiated = false;

        protected override bool TestInitialConditions(Body body)
        {
            // Initial conditions are not strict for a clap, start directly with TestPosture and TestRunningGesture
            return true;
        }

        protected override bool TestPosture(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var handLeft = body.Joints[JointType.HandLeft].Position;

            if (!isClapInitiated)
            {
                previousRightHandPosition = handRight;
                previousLeftHandPosition = handLeft;
                isClapInitiated = true;
                return false; // Wait for the next frame to start evaluation
            }

            // Check if hands moved closer since the last frame
            bool handsMovingCloser =
                (handRight.X - previousRightHandPosition.X) < 0 &&
                (handLeft.X - previousLeftHandPosition.X) > 0;

            // Update positions for next comparison
            previousRightHandPosition = handRight;
            previousLeftHandPosition = handLeft;

            // Consider hand movement towards each other as part of the posture check
            return handsMovingCloser && Math.Abs(handRight.Y - handLeft.Y) < 0.2f;
        }

        protected override bool TestRunningGesture(Body body)
        {
            // This method could be used to check for the continuation of the gesture, if necessary
            return true;
        }

        protected override bool TestEndConditions(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var handLeft = body.Joints[JointType.HandLeft].Position;

            // End condition based on proximity of hands
            return Math.Abs(handRight.X - handLeft.X) < 0.1f;
        }

        public override string GestureName => "Clap Hands";
    }
}
