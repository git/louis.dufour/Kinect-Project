﻿using Microsoft.Kinect;
using System;
using KinectUtils;

namespace MyGesturesBank
{
    public class PostureHandsOnHead : Posture
    {
        protected override bool TestPosture(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var handLeft = body.Joints[JointType.HandLeft].Position;
            var head = body.Joints[JointType.Head].Position;

            // Ajustez les seuils selon la précision souhaitée
            var threshold = 0.1f; // Seuil pour ajuster la précision de la détection

            return Math.Abs(handRight.Y - head.Y) < threshold
                && Math.Abs(handLeft.Y - head.Y) < threshold;
        }

        public override string GestureName => "Posture Hands On Head";
    }
}
