﻿using KinectUtils;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGesturesBank
{
    public class TwoHandsDragon : Posture
    {
        protected override bool TestPosture(Body body)
        {
            var handRight = body.Joints[JointType.HandRight].Position;
            var handLeft = body.Joints[JointType.HandLeft].Position;
            var head = body.Joints[JointType.Head].Position;

            return handRight.Y > head.Y && handLeft.Y > head.Y;
        }

        public override string GestureName => "Two Hands Dragon";
    }
}
