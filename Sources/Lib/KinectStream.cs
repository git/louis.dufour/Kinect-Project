﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Lib
{
    public abstract class KinectStream : INotifyPropertyChanged
    {
        public KinectManager KinectManager { get; private set; }
        private WriteableBitmap bitmap;
        public WriteableBitmap Bitmap
        {
            get { return bitmap; }
            protected set
            {
                if (bitmap != value)
                {
                    bitmap = value;
                    OnPropertyChanged(nameof(Bitmap));
                }
            }
        }
        private Canvas _canvas;
        public Canvas Canvas
        {
            get { return _canvas; }
            protected set
            {
                _canvas = value;
                OnPropertyChanged(nameof(Canvas));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public KinectStream(KinectManager kinectManager)
        {
            KinectManager = kinectManager;
            Canvas = null;
            if (kinectManager.Sensor == null)
            {
                KinectManager.StartSensor();
            }
        }

        public virtual void Start()
        {
            KinectManager.StartSensor();
        }

        public abstract void Stop();
    }
}
