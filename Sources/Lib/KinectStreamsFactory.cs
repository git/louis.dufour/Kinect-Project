﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lib
{
    public class KinectStreamsFactory
    {
        private KinectManager _kinectManager;
        public KinectManager KinectManager
        {
            get { return _kinectManager; }
            private set { _kinectManager = value; }
        }

        private Dictionary<KinectStreams, Func<KinectStream>> _streamFactory;
        public Dictionary<KinectStreams, Func<KinectStream>> StreamFactory
        {
            get { return _streamFactory; }
            private set { _streamFactory = value; }
        }

        public KinectStreamsFactory(KinectManager kinect, Canvas SkeletonCanvas)
        {
            _kinectManager = kinect;
            // Initialisation de la fabrique avec les fonctions de création pour chaque type de flux.
            StreamFactory = new Dictionary<KinectStreams, Func<KinectStream>>
            {
                { KinectStreams.Color, () => new ColorImageStream(KinectManager) },
                { KinectStreams.Depth, () => new DepthImageStream(KinectManager) },
                { KinectStreams.IR, () => new InfraredImageStream(KinectManager) },
                { KinectStreams.Body, () => new BodyImageStream(KinectManager, SkeletonCanvas) },
                {
                    KinectStreams.ColorAndBody,
                    () => new ColorAndBodyImageStream(KinectManager, SkeletonCanvas)
                }
            };
        }

        public KinectStream this[KinectStreams stream]
        {
            get
            {
                if (StreamFactory.ContainsKey(stream))
                {
                    return StreamFactory[stream]();
                }
                else
                {
                    throw new ArgumentException("Invalid stream type.");
                }
            }
        }
    }
}
