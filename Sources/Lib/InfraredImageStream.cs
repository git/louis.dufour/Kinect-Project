﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace Lib
{
    class InfraredImageStream : KinectStream
    {
        private InfraredFrameReader reader;
        private byte[] infraredPixels;
        public byte[] InfraredPixels
        {
            get { return infraredPixels; }
            private set { infraredPixels = value; }
        }
        public InfraredFrameReader Reader
        {
            get { return reader; }
            private set { reader = value; }
        }

        public InfraredImageStream(KinectManager kinectManager)
            : base(kinectManager)
        {
            var frameDescription = KinectManager.Sensor.InfraredFrameSource.FrameDescription;
            this.Bitmap = new WriteableBitmap(
                frameDescription.Width,
                frameDescription.Height,
                96.0,
                96.0,
                PixelFormats.Gray8,
                null
            );
            reader = KinectManager.Sensor.InfraredFrameSource.OpenReader();
            reader.FrameArrived += Reader_InfraredFrameArrived;
            infraredPixels = new byte[frameDescription.Width * frameDescription.Height];
        }

        public override void Stop()
        {
            if (reader != null)
            {
                reader.Dispose();
                reader = null;
            }
        }

        private void ProcessInfraredFrameData(ushort[] frameData, uint frameDataSize)
        {
            // Convertir les données infrarouges en niveaux de gris
            for (int i = 0; i < frameDataSize; ++i)
            {
                // Convertir la valeur infrarouge en une intensité lumineuse
                byte intensity = (byte)(frameData[i] >> 8);
                InfraredPixels[i] = intensity;
            }
        }

        private void Reader_InfraredFrameArrived(object sender, InfraredFrameArrivedEventArgs e)
        {
            using (InfraredFrame infraredFrame = e.FrameReference.AcquireFrame())
            {
                if (infraredFrame != null)
                {
                    FrameDescription infraredFrameDescription = infraredFrame.FrameDescription;

                    // Créez un tableau pour stocker les données infrarouges
                    ushort[] infraredData = new ushort[infraredFrameDescription.LengthInPixels];
                    infraredFrame.CopyFrameDataToArray(infraredData);

                    // Traitez les données infrarouges
                    ProcessInfraredFrameData(infraredData, infraredFrameDescription.LengthInPixels);

                    // Mettez à jour le bitmap infrarouge
                    this.Bitmap.WritePixels(
                        new Int32Rect(
                            0,
                            0,
                            infraredFrameDescription.Width,
                            infraredFrameDescription.Height
                        ),
                        this.infraredPixels,
                        infraredFrameDescription.Width,
                        0
                    );
                }
            }
        }
    }
}
