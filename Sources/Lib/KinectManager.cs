﻿using Microsoft.Kinect;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Media;

namespace Lib
{
    public class KinectManager : INotifyPropertyChanged
    {
        private KinectSensor sensor;
        public KinectSensor Sensor
        {
            get { return sensor; }
            set
            {
                sensor = value;
                OnPropertyChanged(nameof(Sensor));
            }
        }

        private SolidColorBrush _kinectStatusColor = new SolidColorBrush(Colors.Red); // Couleur rouge par défaut, signifiant inactive
        public SolidColorBrush KinectStatusColor
        {
            get { return _kinectStatusColor; }
            set
            {
                _kinectStatusColor = value;
                OnPropertyChanged(nameof(KinectStatusColor));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public KinectManager()
        {
            sensor = KinectSensor.GetDefault();
            UpdateStatusProperties();
            // Abonnez-vous à l'événement IsAvailableChanged
            if (sensor != null)
            {
                sensor.IsAvailableChanged += KinectSensor_IsAvailableChanged;
            }
        }

        public void StartSensor()
        {
            if (sensor != null && !sensor.IsOpen)
            {
                sensor.Open();
            }
            else
            {
                sensor = KinectSensor.GetDefault();
                sensor.Open();
            }
        }

        public void StopSensor()
        {
            if (sensor != null)
            {
                sensor.Close();
                sensor = null;
            }
        }

        public bool Status
        {
            get { return Sensor != null && Sensor.IsAvailable; }
        }
        private string _statusText;
        public string StatusText
        {
            get { return _statusText; }
            set
            {
                _statusText = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                }
            }
        }

        private void KinectSensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            UpdateStatusProperties();
        }

        private void UpdateStatusProperties()
        {
            if (Sensor == null)
            {
                StatusText = "Kinect n'est pas connecté";
                KinectStatusColor = new SolidColorBrush(Colors.Red);
            }
            else if (!Sensor.IsOpen)
            {
                StatusText = "Kinect n'est pas ouvert";
                KinectStatusColor = new SolidColorBrush(Colors.Yellow);
            }
            else if (Sensor.IsAvailable)
            {
                StatusText = "Kinect est disponible";
                KinectStatusColor = new SolidColorBrush(Colors.Green);
            }
            else
            {
                StatusText = "Kinect n'est pas disponible";
                KinectStatusColor = new SolidColorBrush(Colors.Orange);
            }
        }
    }
}
