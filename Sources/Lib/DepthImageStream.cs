﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace Lib
{
    class DepthImageStream : KinectStream
    {
        private DepthFrameReader reader;
        public DepthFrameReader Reader
        {
            get { return reader; }
            set { reader = value; }
        }
        private byte[] depthPixels;
        public byte[] DepthPixels
        {
            get { return depthPixels; }
            set { depthPixels = value; }
        }

        public DepthImageStream(KinectManager kinectManager)
            : base(kinectManager)
        {
            var frameDescription = KinectManager.Sensor.DepthFrameSource.FrameDescription;
            this.Bitmap = new WriteableBitmap(
                frameDescription.Width,
                frameDescription.Height,
                96.0,
                96.0,
                PixelFormats.Gray8,
                null
            );
            reader = KinectManager.Sensor.DepthFrameSource.OpenReader();
            reader.FrameArrived += Reader_DepthFrameArrived;
            DepthPixels = new byte[frameDescription.Width * frameDescription.Height];
        }

        private void Reader_DepthFrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            using (DepthFrame depthFrame = e.FrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    FrameDescription depthFrameDescription = depthFrame.FrameDescription;

                    // Créez un tableau pour stocker les données de profondeur
                    ushort[] depthData = new ushort[depthFrameDescription.LengthInPixels];
                    depthFrame.CopyFrameDataToArray(depthData);

                    // Traitez les données de profondeur
                    ProcessDepthFrameData(
                        depthData,
                        depthFrameDescription.LengthInPixels,
                        depthFrame.DepthMinReliableDistance,
                        depthFrame.DepthMaxReliableDistance
                    );

                    // Mettez à jour le bitmap de profondeur
                    this.Bitmap.WritePixels(
                        new Int32Rect(
                            0,
                            0,
                            depthFrameDescription.Width,
                            depthFrameDescription.Height
                        ),
                        this.depthPixels,
                        depthFrameDescription.Width,
                        0
                    );
                }
            }
        }

        public override void Stop()
        {
            if (reader != null)
            {
                reader.Dispose();
                reader = null;
            }
        }

        private void ProcessDepthFrameData(
            ushort[] depthData,
            uint depthFrameDataSize,
            ushort minDepth,
            ushort maxDepth
        )
        {
            // Convertir les données de profondeur en niveaux de gris
            for (int i = 0; i < depthFrameDataSize; ++i)
            {
                ushort depth = depthData[i];
                DepthPixels[i] = (byte)(depth >= minDepth && depth <= maxDepth ? (depth % 256) : 0);
            }
        }
    }
}
